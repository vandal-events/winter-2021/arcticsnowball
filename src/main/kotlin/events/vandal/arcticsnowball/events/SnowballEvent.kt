package events.vandal.arcticsnowball.events

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.SoundCategory
import org.bukkit.entity.Player
import org.bukkit.entity.Snowball
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.player.PlayerDropItemEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import events.vandal.arcticsnowball.ArcticSnowball
import events.vandal.arcticsnowball.streak.StreakManager

object SnowballEvent : Listener {
    @EventHandler
    fun onSnowballThrow(ev: PlayerInteractEvent) {
        if ((ev.action == Action.RIGHT_CLICK_AIR || ev.action == Action.RIGHT_CLICK_BLOCK) && (ev.hasItem() && ev.item?.type == Material.SNOWBALL)) {
            if (ev.player.hasCooldown(Material.SNOWBALL))
                return

            if (!ArcticSnowball.snowballsEnabled) {
                ev.isCancelled = true
                return
            }

            if (ev.player.hasPermission("arcticsnowball.exempt") || ev.player.hasPermission("arcticsnowball.exempt-op")) {
                ev.isCancelled = true
                ev.player.sendMessage("${ChatColor.RED}>> You are currently exempt from using snowballs!")

                return
            }

            //ev.setUseItemInHand(Event.Result.ALLOW)
            ev.isCancelled = true
            ev.player.setCooldown(Material.SNOWBALL, ArcticSnowball.SNOWBALL_COOLDOWN)
            ev.player.launchProjectile(Snowball::class.java)
        }
    }

    @EventHandler
    fun onSnowballDrop(ev: PlayerDropItemEvent) {
        if (ev.itemDrop.itemStack.type == Material.SNOWBALL) {
            ev.isCancelled = true
        }
    }

    @EventHandler
    fun onPlayerLogin(ev: PlayerJoinEvent) {
        if ((!ev.player.inventory.isEmpty && ev.player.inventory.contains(Material.SNOWBALL)) || ev.player.hasPermission("arcticsnowball.exempt")) return

        ev.player.inventory.addItem(ItemStack(Material.SNOWBALL, 1))
    }

    @EventHandler
    fun onPlayerHitBySnowball(ev: EntityDamageByEntityEvent) {
        if (ev.entity !is Player) return
        if (ev.damager !is Snowball) return
        if (!ArcticSnowball.snowballsEnabled) {
            ev.isCancelled = true
            return
        }

        val snowball = ev.damager as Snowball

        if (ev.entity.hasPermission("arcticsnowball.exempt") || ev.entity.hasPermission("arcticsnowball.exempt-op")) {
            ev.isCancelled = true
            ev.damager.sendMessage("${ChatColor.RED}>> ${ChatColor.YELLOW}${ev.entity.name}${ChatColor.WHITE} does not have snowballs enabled!")

            return
        }

        if (snowball.shooter is Player) {
            val player = snowball.shooter as Player
            // Apply knockback, from https://dev.bukkit.org/projects/projectile-knockback
            // I actually didn't think this worked, but it did surprisingly
            ev.damage = 0.000001

            ev.entity.sendMessage("${ChatColor.RED}>> ${ChatColor.YELLOW}${player.name}${ChatColor.WHITE} has hit you with their snowball!")
            player.sendMessage("${ChatColor.GREEN}>> ${ChatColor.WHITE}You hit ${ChatColor.YELLOW}${ev.entity.name} ${ChatColor.GREEN}with your snowball!")
            player.playSound(player.location, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, SoundCategory.PLAYERS, 1.0F, 2.0F)

            StreakManager.addToStreak(player)
            StreakManager.resetStreak(ev.entity as Player)
        }
    }

    // literally can't destroy anything
    @EventHandler
    fun preventAnyKindOfDamage(ev: EntityDamageByEntityEvent) {
        if (ev.damager is Snowball) return
        // this is probably good for stuff
        if (ev.damager.isOp) return

        ev.isCancelled = true
    }
}